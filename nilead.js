/**
 * Created by Rubikin Team.
 * ========================
 *
 * This file is a part of Nilead project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

'use strict';

// jscs:disable
// jshint ignore: start

let fs = require('fs');
let path = require('path');
let gulp = require('gulp');
let exec = require('child_process').exec;

module.exports = Nilead;

function Nilead (gulpfile) {
    this.gulpfile = gulpfile;
    this.timer = null;
}

Nilead.prototype.run = function() {
    let processor = exec('bash ' + __dirname + '/gulp.sh ' + __dirname + ' ' + this.gulpfile);

    processor.stdout.on('data', this.log.bind(this));
    processor.stderr.on('data', this.error.bind(this));
};

Nilead.prototype.log = function(message, lf) {
    clearTimeout(this.timer);
    this.timer = setTimeout(console.log, 3000, '');

    message = message.replace(/[\r\n]/g, '').trim();
    var segments = message.match(/^(.*?)\[\d{2}\:\d{2}\:\d{2}\](.*?)$/);

    if (!message || 0 === message.search('Using gulpfile') || 0 === message.search('Working')) {
        return;
    }

    if (segments) {
        this.log(segments[1]);
        this.log(segments[2]);

        return;
    }

    var time = new Date().toISOString().substr(11, 12);

    message = message
        .replace(/^Starting/, '- Starting')
        .replace(/^Finished/, '+ Finished')
    ;

    console.log('[' + time + ']', message);
};

Nilead.prototype.error = function(message) {
    console.log('');
    console.log('--------------------------------------------------------------------------------');
    console.error(message.trim());
    console.log('--------------------------------------------------------------------------------');
    console.log('');
};
