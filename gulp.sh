#!/bin/bash

ROOT=$1
GULPFILE=$2

gulp --cwd="$ROOT" --gulpfile="$GULPFILE"
